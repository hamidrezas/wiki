This library is placed in /application/libraries/Speedup and has some classes which will be described here. Also you can take a look at manifest.php which is placed inside this folder.

### Speedup_Base ###
You can simply use this static method to encrypt your string with an custom key everywhere in your scripts:
```
#!php

Speedup_Base::encrypt(string, key)
```
Also you can use this static method to decrypt such a string:
```
#!php

Speedup_Base::decrypt(string, key)
```
