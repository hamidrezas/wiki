This controller lets users to pick a custom theme for themselves from a specified list. It has two actions, **index** which contains theme list and **perfortm** which perform the act of changing the theme.

To have this controller working, we have to add a new column to user table:

```
#!sql

ALTER TABLE `engine4_users` ADD `theme` varchar(10) NULL DEFAULT '0';
```

**Important note**:
There is a file in *application/modules/Core/layouts/scripts* which is responsible for assigning specified theme to show.
Insert following snippet on line ~89 and delete previous lines.

```
#!php

if( Engine_Api::_()->user()->getViewer()->getIdentity())
{
	$viewerId  = Engine_Api::_()->user()->getViewer()->getIdentity();
	$userDbTable = Engine_Api::_()->getDbtable('users', 'user');
	$row = $userDbTable->find($viewerId)->current();
	$userTheme = array($row->theme);
	$themes = $userTheme;
} else {
	$themes = array('clean');
}
```
Have fun!