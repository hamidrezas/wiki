Socialengine uses Nginx as front-end server and Apache with PHP-FPM as Back-end Server.
The following chart describe the scenario clearly:

#### Static files   ---> Nginx ####
#### PHP files      ---> Nginx   ---> PHP-FPM  // fastcgi_pass module ####
#### Htaccess       ---> Nginx    ---> Apache   // reverse_proxy module ####




To have this plan working we have first run:
#### Nginx   (listen on port 80) ####
#### Apache  (listen on port 9001) ####
#### PHP-FPM (listen on port 9000) ####



After running these daemon you have to create a two VirtualHosts for both Apache and Nginx:
## Nginx ##

```
#!php

server {
        listen *:80;       
        server_name friendup.ir www.friendup.ir;
        root   /var/www/friendup.ir/web;		
        index index.html index.htm index.php index.cgi index.pl index.xhtml;
				
        error_page 400 /error/400.html;
        error_page 401 /error/401.html;
        error_page 403 /error/403.html;
        error_page 404 @backend;
        error_page 405 /error/405.html;
        error_page 500 /error/500.html;
        error_page 502 /error/502.html;
        error_page 503 /error/503.html;
        recursive_error_pages on;
        location = /error/400.html {
            internal;
        }
        location = /error/401.html {
            internal;
        }
        location = /error/403.html {
            internal;
        }
        location = /error/404.html {
            internal;
        }
        location = /error/405.html {
            internal;
        }
        location = /error/500.html {
            internal;
        }
        location = /error/502.html {
            internal;
        }
        location = /error/503.html {
            internal;
        }
		
        error_log /var/log/ispconfig/httpd/friendup.ir/error.log;
        access_log /var/log/ispconfig/httpd/friendup.ir/access.log combined;

        ## Disable .htaccess and other hidden files
        location ~ /\. {
            deny all;
            access_log off;
            log_not_found off;
        }
		
        location = /favicon.ico {
            log_not_found off;
            access_log off;
        }

        location = /robots.txt {
            allow all;
            log_not_found off;
            access_log off;
        }
		
        location /stats {
            index index.html index.php;
            auth_basic "Members Only";
            auth_basic_user_file /var/www/clients/client0/web1/.htpasswd_stats;
        }

        location ^~ /awstats-icon {
            alias /usr/share/awstats/icon;
        }
		
		# This block servers php files and pass them to
		# PHP-FPM which is listening on port 9000
        location ~ \.php$ {
            try_files $uri =404;
            include /etc/nginx/fastcgi_params;
            fastcgi_pass 127.0.0.1:9000;
            fastcgi_index index.php;
            fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
            fastcgi_param PATH_INFO $fastcgi_script_name;
            fastcgi_intercept_errors on;
        }
		
      # This block serves static files by Nginx 
	  # and if couldn't find them pass to @backend block
	  # where apache is waiting
	  location / { 
            # try_files attempts to serve a file or folder, until it reaches the fallback at the end
            try_files $uri @backend;
      } 
	  
	  # This block pass requests to Apache which is listening on port 9001
      location @backend {
			# Sends client IP to Apache
            proxy_set_header X-Real-IP  $remote_addr;
            proxy_set_header X-Forwarded-For $remote_addr;
            proxy_set_header Host $host;
            proxy_pass http://127.0.0.1:9001;
      }
	
}
```

## Apache ##

```
#!php

<VirtualHost *:9001>
      DocumentRoot "/var/www/friendup.ir/web"
      ServerName friendup.ir
      ServerAlias www.friendup.ir
		<Directory "/var/www/friendup.ir">
			# Deny all requests excepts the ones from local which is Nginx calls
			AllowOverride All
		   Order Deny,Allow
		   Deny from all
		   Allow from 127.0.0.1
		</Directory>
      CustomLog /var/log/httpd/friendup_access.log common
      ErrorLog /var/log/httpd/friendup_error.log
</VirtualHost>
```

**Note:** Consider that in this case our domain is www.friendup.ir and the host location is /var/www/friendup.ir/web